package com.alexshelepov.linksloader.network;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey on 02.10.2016.
 */

public abstract class LinksLoaderAsyncTask extends AsyncTask<String, Void, List<String>> {

    private final String TAG = "LinksLoaderAsyncTask";

    private ILoadingListener mLoadingListener = null;
    private Exception mException = null;

    public LinksLoaderAsyncTask(ILoadingListener mLoadingListener) {
        this.mLoadingListener = mLoadingListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mLoadingListener != null) {
            mLoadingListener.onLoadStarted();
        }
    }

    @Override
    protected List<String> doInBackground(String... urls) {
        List<String> links = null;
        try {
            Log.d(TAG, "load url: " +urls[0]);
            links = getLinks(urls[0]);
        } catch (Exception e) {
            e.printStackTrace();
            setException(e);
        }
        return links;
    }

    protected abstract List<String> getLinks(String url) throws Exception;

    @Override
    protected void onPostExecute(List<String> strings) {
        super.onPostExecute(strings);
        if(mException != null) {
            onException(mException);
        }
        else onLoaded(strings);
    }

    @Override
    protected void onCancelled(List<String> strings) {
        super.onCancelled(strings);
        Log.d(TAG, "onCancelled()");
    }


    public void cancelLoading() {
        cancel(true);
        if (mLoadingListener != null) {
            mLoadingListener.onCancelled();
        }
    }

    private void onLoaded(List<String> strings) {
        if (mLoadingListener != null) {
            mLoadingListener.onLoaded(strings);
        }
    }

    private void onException(Exception e) {
        if (mLoadingListener != null) {
            mLoadingListener.onException(e);
        }
    }

    protected void setException(Exception e) {
        mException = e;
    }

}

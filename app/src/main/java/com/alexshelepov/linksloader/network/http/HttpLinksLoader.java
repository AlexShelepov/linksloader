package com.alexshelepov.linksloader.network.http;

import android.os.AsyncTask;
import android.util.Patterns;
import android.webkit.URLUtil;

import com.alexshelepov.linksloader.network.ILinksFilter;
import com.alexshelepov.linksloader.network.ILinksLoader;
import com.alexshelepov.linksloader.network.ILoadingListener;
import com.alexshelepov.linksloader.network.LinksLoaderAsyncTask;

/**
 * Created by Alexey on 30.09.2016.
 */

public class HttpLinksLoader implements ILinksLoader {

    private LinksLoaderAsyncTask mLoaderTask;

    public HttpLinksLoader() {
        mLoaderTask = null;
    }

    @Override
    public boolean isValidUrl(String url) {
        return (URLUtil.isHttpUrl(url) || URLUtil.isHttpsUrl(url))
                && Patterns.WEB_URL.matcher(url).matches();
    }

    @Override
    public void loadLink(String url, ILoadingListener loadingListener) {
        if(mLoaderTask != null) mLoaderTask.cancelLoading();
        mLoaderTask = new HttpJsoupLoaderAsyncTask(loadingListener, new ILinksFilter() {
            @Override
            public boolean test(String link) {
                return isValidUrl(link);
            }
        });
        mLoaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }

}

package com.alexshelepov.linksloader.network;

import com.alexshelepov.linksloader.network.http.HttpLinksLoader;

/**
 * Created by Alexey on 30.09.2016.
 */
public class LinksLoaderManager {
    private static LinksLoaderManager ourInstance = null;

    private ILinksLoader mLinksLoader;

    private LinksLoaderManager() {
        mLinksLoader = null;
    }

    public static LinksLoaderManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new LinksLoaderManager();
        }
        return ourInstance;
    }


    public ILinksLoader getLinksLoader() {
        if(mLinksLoader == null) {
            mLinksLoader = new HttpLinksLoader();
        }

        return mLinksLoader;
    }
}

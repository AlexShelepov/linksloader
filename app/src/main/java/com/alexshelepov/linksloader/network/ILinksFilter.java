package com.alexshelepov.linksloader.network;

/**
 * Created by Alexey on 05.10.2016.
 */

public interface ILinksFilter {

    public boolean test(String link);

}

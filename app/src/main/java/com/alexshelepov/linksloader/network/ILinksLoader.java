package com.alexshelepov.linksloader.network;

/**
 * Created by Alexey on 29.09.2016.
 */

public interface ILinksLoader {

    public boolean isValidUrl(String url);

    public void loadLink(String url, ILoadingListener loadingListener);

}

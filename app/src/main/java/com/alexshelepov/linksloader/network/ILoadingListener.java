package com.alexshelepov.linksloader.network;

import java.util.List;

/**
 * Created by Alexey on 29.09.2016.
 */

public interface ILoadingListener {

    public void onLoadStarted();

    public void onLoaded(List<String> links);

    public void onException(Exception e);

    public void onCancelled();

}

package com.alexshelepov.linksloader.network.http;

import com.alexshelepov.linksloader.network.ILinksFilter;
import com.alexshelepov.linksloader.network.ILoadingListener;
import com.alexshelepov.linksloader.network.LinksLoaderAsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey on 02.10.2016.
 */

public class HttpJsoupLoaderAsyncTask extends LinksLoaderAsyncTask {

    private ILinksFilter mLinksFilter;

    public HttpJsoupLoaderAsyncTask(ILoadingListener mLoadingListener, ILinksFilter linksFilter) {
        super(mLoadingListener);

        if(linksFilter == null) {
            mLinksFilter = new ILinksFilter() {
                @Override
                public boolean test(String link) {
                    return true;
                }
            };
        } else {
            mLinksFilter = linksFilter;
        }
    }

    @Override
    protected List<String> getLinks(String url) throws Exception {
        List<String> resultLinks = new ArrayList<>();
        Document doc = Jsoup.connect(url).get();
        if(isCancelled()) return resultLinks;
        Elements links = doc.select("a[href]");
        for (Element link : links) {
            if(isCancelled()) return resultLinks;
            String linkstr = link.attr("abs:href").trim();
            if (linkstr.length() > 0 && mLinksFilter.test(linkstr))
                resultLinks.add(linkstr);
        }

        return resultLinks;
    }
}

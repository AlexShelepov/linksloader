package com.alexshelepov.linksloader.network.http;

import android.util.Log;

import com.alexshelepov.linksloader.network.ILoadingListener;
import com.alexshelepov.linksloader.network.LinksLoaderAsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey on 02.10.2016.
 */

public class HttpNativeLoaderAsyncTask extends LinksLoaderAsyncTask {

    private final String TAG = "LinksLoaderAsyncTask";

    public HttpNativeLoaderAsyncTask(ILoadingListener mLoadingListener) {
        super(mLoadingListener);
    }

    protected List<String> getLinks(String myurl) throws IOException {
        InputStream is = null;
        List<String> links = new ArrayList<>();

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 );
            conn.setConnectTimeout(15000 );
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "Content type is: " + conn.getContentType() );
            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();
            //todo parse links from is

            return links;
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

}
package com.alexshelepov.linksloader.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.alexshelepov.linksloader.R;

import org.jsoup.HttpStatusException;
import org.jsoup.UnsupportedMimeTypeException;

import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by Alexey on 02.10.2016.
 */

public class DialogUtil {

    public static void showErrorToast(Context context, Exception e) {
        int message;
        if(!NetworkUtils.isNetworkEnabled(context)) {
            message = R.string.error_check_network;
        } else if(e instanceof UnknownHostException) {
            message = R.string.error_unknown_host;
        } else if (e instanceof MalformedURLException) {
            message = R.string.error_unsupported_url;
        } else if (e instanceof UnsupportedMimeTypeException) {
            message = R.string.error_unsupproted_mimetype;
        } else if (e instanceof SocketTimeoutException) {
            message = R.string.error_socket_timeout;
        }  else if (e instanceof HttpStatusException) {
            int statusCode = ((HttpStatusException) e).getStatusCode();
            if(statusCode >= 500 && statusCode < 600) {
                // server error
                message = R.string.error_http_server_error;
            } else if(statusCode >= 400 && statusCode < 500) {
                // can't open url
                message = R.string.error_http_4xx;
            } else {
                message = R.string.error_message;
            }
        } else {
            message = R.string.error_message;
        }

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static AlertDialog error(Context ctx, String message) {
        return alert(ctx, R.string.error, message, true);
    }

    /**
     *
     */
    public static AlertDialog alert(Context ctx, int title, String message, boolean cancelable) {
        try {
            final AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();
            alertDialog.setTitle(title);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, ctx.getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.cancel();
                }
            });
            alertDialog.setMessage(message);
            alertDialog.setCancelable(cancelable);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            return alertDialog;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}

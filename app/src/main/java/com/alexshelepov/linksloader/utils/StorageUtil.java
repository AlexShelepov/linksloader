package com.alexshelepov.linksloader.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.alexshelepov.linksloader.Constants;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Alexey on 03.10.2016.
 */

public class StorageUtil {

    public static final String PREFS_NAME = "user_data";

    private SharedPreferences mSharedPreferences;

    private static StorageUtil mInstance = null;

    private StorageUtil(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static StorageUtil getInstance(Context context) {
        if(mInstance == null) mInstance = new StorageUtil(context);

        return mInstance;
    }

    public void saveUrl(String url) {
        mSharedPreferences.edit().putString(Constants.URL_KEY, url).apply();
    }

    public String restoreUrl() {
        return mSharedPreferences.getString(Constants.URL_KEY, Constants.DEFAULT_URL);
    }

    public void saveLinks(ArrayList<String> links) {
        Editor editor = mSharedPreferences.edit();
        try {
            editor.putString(Constants.LINKS_KEY, ObjectSerializer.serialize(links));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    public ArrayList<String> restoreLinks() {
        ArrayList<String> links = null;
        try {
            String restoredString = mSharedPreferences.getString(Constants.LINKS_KEY, null);
            Object deserializedList = ObjectSerializer.deserialize(restoredString);
            if(deserializedList != null) {
                links = (ArrayList<String>)deserializedList;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return links;
    }



}

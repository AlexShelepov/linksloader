package com.alexshelepov.linksloader;

/**
 * Created by Alexey on 03.10.2016.
 */

public class Constants {

    public static final String LINKS_KEY = "links_list";
    public static final String URL_KEY = "url";

    public static final String DEFAULT_URL = "";

}

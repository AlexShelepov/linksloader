package com.alexshelepov.linksloader;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.alexshelepov.linksloader.network.ILoadingListener;
import com.alexshelepov.linksloader.network.LinksLoaderManager;
import com.alexshelepov.linksloader.utils.DialogUtil;
import com.alexshelepov.linksloader.utils.StorageUtil;

import java.util.ArrayList;
import java.util.List;

public class LinksLoaderActivity extends AppCompatActivity {

    private static final String TAG = "LinksLoaderActivity";

    private ProgressBar mProgressBar;
    private EditText mUrlEditText;
    private Button mGetLinksButton;
    private ListView mLinksList;
    private ArrayList<String> mLinks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() " +savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinks = new ArrayList<>();
        initViews();
        restoreValues(savedInstanceState);
    }

    private void restoreValues(Bundle savedInstanceState) {
        String url = Constants.DEFAULT_URL;
        mLinks.clear();
        if(savedInstanceState != null) {
            if (savedInstanceState.containsKey(Constants.URL_KEY)) {
                url = savedInstanceState.getString(Constants.URL_KEY);
            }
            if (savedInstanceState.containsKey(Constants.LINKS_KEY)) {
                mLinks.addAll(savedInstanceState.getStringArrayList(Constants.LINKS_KEY));
            }
        } else {
            StorageUtil storage = StorageUtil.getInstance(this);
            url = storage.restoreUrl();
            List<String> restoredLinks = storage.restoreLinks();
            if(restoredLinks != null) {
                mLinks.addAll(restoredLinks);
            }
        }

        mUrlEditText.setText(url);
        ((ArrayAdapter) mLinksList.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "onConfigurationChanged() " +newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.URL_KEY, mUrlEditText.getText().toString());
        outState.putStringArrayList(Constants.LINKS_KEY, mLinks);
        Log.d(TAG, "onSaveInstanceState()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        StorageUtil storage = StorageUtil.getInstance(this);
        storage.saveUrl(mUrlEditText.getText().toString());
    }

    private void initViews() {
        initToolbar();

        mUrlEditText = (EditText) findViewById(R.id.urlEditText);
        setHttpUrlTextChangeListener(mUrlEditText);
        mGetLinksButton = (Button) findViewById(R.id.getLinksButton);
        setGetLinksButtonOnClickListener(mGetLinksButton);

        mLinksList = (ListView) findViewById(R.id.linksList);
        mLinksList.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mLinks));
        mLinksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = mLinks.get(position);
                mUrlEditText.setText(url);
                if(!LinksLoaderManager.getInstance().getLinksLoader().isValidUrl(url)) {
                    Log.d(TAG, "invalid link: " +url);
                }
            }
        });
    }

    private void initToolbar() {
        Toolbar appToolbar = (Toolbar) findViewById(R.id.appToolbar);
        setSupportActionBar(appToolbar);
        appToolbar.setTitle(R.string.app_name);
        mProgressBar = (ProgressBar) findViewById(R.id.toolbarProgressBar);
    }

    private void setGetLinksButtonOnClickListener(Button getLinksButton) {
        getLinksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadLinks(mUrlEditText.getText().toString());
                //mUrlEditText.setText("");
                v.setEnabled(false);
            }
        });
    }

    private void loadLinks(String url) {
        LinksLoaderManager.getInstance().getLinksLoader().loadLink(url, new ILoadingListener() {
            @Override
            public void onLoadStarted() {
                showProgress();
            }

            @Override
            public void onLoaded(List<String> links) {
                mLinks.clear();
                if(links != null) {
                    mLinks.addAll(links);
                    StorageUtil storage = StorageUtil.getInstance(LinksLoaderActivity.this);
                    storage.saveLinks(mLinks);
                }
                ((ArrayAdapter) mLinksList.getAdapter()).notifyDataSetChanged();
                hideProgress();
            }

            @Override
            public void onCancelled() {
                hideProgress();
            }

            @Override
            public void onException(Exception e) {
                DialogUtil.showErrorToast(LinksLoaderActivity.this, e);
                hideProgress();
            }
        });
    }

    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void setHttpUrlTextChangeListener(EditText urlEditText) {
        urlEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(LinksLoaderManager.getInstance().getLinksLoader().isValidUrl(s.toString())) {
                    mGetLinksButton.setEnabled(true);
                } else {
                    mGetLinksButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


}
